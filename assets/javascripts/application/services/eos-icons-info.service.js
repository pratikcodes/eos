const getMoreInfoFromIconService = (icon, callback) => { // eslint-disable-line no-unused-vars
  $.when(
    $.ajax({
      url: `/api/icons`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.log(`there was an error retrieving the icon information`)
        callback()
      }
    }))
    .then(function (data) {
      callback(data.filter(ele => ele.name === icon).reduce((acc, cur) => cur, {}))
    })
}
